﻿using GTA;
using GTA.Math;
using GTA.Native;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Media;
using System.Windows.Forms;
using Control = GTA.Control;
namespace SessionMarker
{
	public class Main : Script
	{
		//handle key states myself, because SHVDN has no "just pressed" for keys
		class KeyStates
		{
			internal bool placeKey1State, placeKey2State, gotoKey1State, gotoKey2State;
		}

		readonly VehicleModType[] MOD_TYPES = (VehicleModType[])Enum.GetValues(typeof(VehicleModType));

		readonly bool saveVehicle, takeVehicle, takeVehicleIfSaved, saveVelocity, playSounds, playSoundsWhenStartHold, drawStaticWhenHoldingGoto;
		readonly bool holdPlace, holdGoto;
		readonly int placeHoldTime, gotoHoldTime;
		readonly Keys placeKey1, placeKey2, gotoKey1, gotoKey2;
		readonly bool oneKeyPlace, oneKeyGoto;
		readonly Control placeControl1, placeControl2, gotoControl1, gotoControl2;
		readonly bool oneControlPlace, oneControlGoto;

		KeyStates lastStates = new KeyStates();
		KeyStates curStates = new KeyStates();

		bool canUsePlace, canUseGoto;

		SoundPlayer placeMarkerSound = new SoundPlayer("./scripts/SessionMarker/place-marker.wav"), gotoMarkerSound = new SoundPlayer("./scripts/SessionMarker/goto-marker.wav");
		Stopwatch placeStopwatch = new Stopwatch(), gotoStopwatch = new Stopwatch();

		bool markerSaved = false;
		Vector3 pos;
		float heading, camHeading, camPitch;
		int interior;
		uint roomKey;

		bool wasInVehicle;
		Vector3 vehVel;
		Model savedVehModel;
		bool savedVehRunning;
		VehicleColor savedVehPrimaryColor, savedVehSecondaryColor;
		int savedVehLivery;
		VehicleLandingGearState savedVehLandingGear;
		float savedVehNozzlePos;
		List<int> savedVehModIndices = new List<int>();

		public Main()
		{
			saveVehicle = Settings.GetValue("SETTINGS", "SAVE_VEHICLE", true);
			takeVehicle = Settings.GetValue("SETTINGS", "TAKE_VEHICLE", false);
			takeVehicleIfSaved = Settings.GetValue("SETTINGS", "TAKE_VEHICLE_IF_SAVED", false);
			saveVelocity = Settings.GetValue("SETTINGS", "SAVE_VEH_VELOCITY", true);
			playSounds = Settings.GetValue("SETTINGS", "PLAY_SOUNDS", true);
			playSoundsWhenStartHold = Settings.GetValue("SETTINGS", "PLAY_SOUNDS_WHEN_START_HOLD", true);
			drawStaticWhenHoldingGoto = Settings.GetValue("SETTINGS", "DRAW_STATIC_WHEN_HOLDING_GOTO", true);
			placeHoldTime = Math.Max(Settings.GetValue("SETTINGS", "HOLD_PLACE_TIME", 0), 0);
			gotoHoldTime = Math.Max(Settings.GetValue("SETTINGS", "HOLD_GOTO_TIME", 200), 0);
			holdPlace = placeHoldTime > 0;
			holdGoto = gotoHoldTime > 0;

			placeKey1 = Settings.GetValue("KEYS", "PLACE_KEY_1", Keys.LShiftKey);
			placeKey2 = Settings.GetValue("KEYS", "PLACE_KEY_2", Keys.Z);
			gotoKey1 = Settings.GetValue("KEYS", "GOTO_KEY_1", Keys.LShiftKey);
			gotoKey2 = Settings.GetValue("KEYS", "GOTO_KEY_2", Keys.X);
			oneKeyPlace = placeKey2 == Keys.None;
			oneKeyGoto = gotoKey2 == Keys.None;

			placeControl1 = Settings.GetValue("CONTROLS", "PLACE_CONTROL_1", Control.ScriptRB);
			placeControl2 = Settings.GetValue("CONTROLS", "PLACE_CONTROL_2", Control.ScriptPadDown);
			gotoControl1 = Settings.GetValue("CONTROLS", "GOTO_CONTROL_1", Control.ScriptRB);
			gotoControl2 = Settings.GetValue("CONTROLS", "GOTO_CONTROL_2", Control.ScriptPadRight);
			oneControlPlace = placeControl2 == (Control)(-1);
			oneControlGoto = gotoControl2 == (Control)(-1);

			placeMarkerSound.Load();
			gotoMarkerSound.Load();

			Interval = 0;
			Tick += Main_Tick;
		}

		private void Main_Tick(object sender, EventArgs e)
		{
			UpdateStates();
			HandleControls();
		}

		private void UpdateStates()
		{
			lastStates = curStates; //current states are now old, so move them to lastStates

			//update states
			curStates = new KeyStates()
			{
				placeKey1State = Game.IsKeyPressed(placeKey1),
				placeKey2State = Game.IsKeyPressed(placeKey2),
				gotoKey1State = Game.IsKeyPressed(gotoKey1),
				gotoKey2State = Game.IsKeyPressed(gotoKey2)
			};
		}

		private void HandleControls()
		{
			if ((Game.LastInputMethod == InputMethod.MouseAndKeyboard && curStates.placeKey1State && (oneKeyPlace || curStates.placeKey2State)) || //keyboard
				(Game.LastInputMethod == InputMethod.GamePad && Game.IsControlPressed(placeControl1) && (oneControlPlace || Game.IsControlPressed(placeControl2)))) //controller
			{
				if (holdPlace)
				{
					if (canUsePlace)
					{
						if (placeStopwatch.IsRunning)
						{
							if (placeStopwatch.ElapsedMilliseconds >= placeHoldTime)
							{
								canUsePlace = false; //wait until let go of controls before allowing to hold again
								placeStopwatch.Reset();
								PlaceMarker();
							}
						}
						else
						{
							placeStopwatch.Start();

							if (playSounds && playSoundsWhenStartHold) placeMarkerSound.Play();
						}
					}
				}
				else
				{
					if ((lastStates.placeKey1State != curStates.placeKey1State || lastStates.placeKey2State != curStates.placeKey2State) || //keys just pressed
						((oneControlPlace && Game.IsControlJustPressed(placeControl1)) || (Game.IsControlPressed(placeControl1) && Game.IsControlJustPressed(placeControl2)))) //controls just pressed
					{
						PlaceMarker();
					}
				}
			}
			else
			{
				canUsePlace = true;

				if (holdPlace && placeStopwatch.IsRunning)
				{
					placeStopwatch.Reset();

					if (playSounds && playSoundsWhenStartHold) placeMarkerSound.Stop();
				}
			}

			if ((Game.LastInputMethod == InputMethod.MouseAndKeyboard && curStates.gotoKey1State && (oneKeyGoto || curStates.gotoKey2State)) || //keyboard
				(Game.LastInputMethod == InputMethod.GamePad && Game.IsControlPressed(gotoControl1) && (oneControlGoto || Game.IsControlPressed(gotoControl2)))) //controller
			{
				if (holdGoto)
				{
					if (canUseGoto)
					{
						if (gotoStopwatch.IsRunning)
						{
							if (gotoStopwatch.ElapsedMilliseconds >= gotoHoldTime)
							{
								canUseGoto = false;
								gotoStopwatch.Reset();
								GotoMarker();
							}
						}
						else if (Game.Player.Character.Position.DistanceTo(pos) > 2f) //don't TP if already near the marker
						{
							gotoStopwatch.Start();

							if (playSounds && playSoundsWhenStartHold) gotoMarkerSound.Play();
							if (drawStaticWhenHoldingGoto) Static.FadeIn(gotoHoldTime);
						}
					}
				}
				else
				{
					if ((lastStates.gotoKey1State != curStates.gotoKey1State || lastStates.gotoKey2State != curStates.gotoKey2State) || //keys just pressed
						((oneControlGoto && Game.IsControlJustPressed(gotoControl1)) || (Game.IsControlPressed(gotoControl1) && Game.IsControlJustPressed(gotoControl2)))) //controls just pressed
					{
						GotoMarker();
					}
				}
			}
			else
			{
				canUseGoto = true;

				if (holdGoto && gotoStopwatch.IsRunning)
				{
					gotoStopwatch.Reset();

					if (playSounds && playSoundsWhenStartHold) gotoMarkerSound.Stop();
					if (drawStaticWhenHoldingGoto) Static.Cancel();
				}
			}
		}

		private void PlaceMarker()
		{
			if (playSounds && (!holdPlace || !playSoundsWhenStartHold)) placeMarkerSound.Play();

			Ped plrPed = Game.Player.Character;

			markerSaved = true;
			pos = plrPed.Position;
			heading = plrPed.Heading;

			if (saveVehicle)
			{
				wasInVehicle = plrPed.IsInVehicle();

				if (wasInVehicle)
				{
					Vehicle veh = plrPed.CurrentVehicle;

					savedVehModel = veh.Model;
					savedVehRunning = veh.IsEngineRunning;
					savedVehPrimaryColor = veh.Mods.PrimaryColor;
					savedVehSecondaryColor = veh.Mods.SecondaryColor;
					savedVehLivery = veh.Mods.Livery;
					savedVehLandingGear = veh.LandingGearState;
					savedVehNozzlePos = Function.Call<float>(Hash._​GET_​VEHICLE_​FLIGHT_​NOZZLE_​POSITION, veh);

					savedVehModIndices.Clear();

					foreach (var modType in MOD_TYPES)
					{
						savedVehModIndices.Add(veh.Mods[modType].Index);
					}

					if (saveVelocity) vehVel = veh.Velocity;
				}
			}
			else if (takeVehicle)
			{
				wasInVehicle = plrPed.IsInVehicle();
				if (wasInVehicle && saveVelocity) vehVel = plrPed.CurrentVehicle.Velocity;
			}

			interior = Function.Call<int>(Hash.GET_INTERIOR_FROM_ENTITY, plrPed);
			roomKey = Function.Call<uint>(Hash.GET_ROOM_KEY_FROM_ENTITY, plrPed);
			camHeading = GameplayCamera.RelativeHeading;
			camPitch = GameplayCamera.RelativePitch;
		}

		private void GotoMarker()
		{
			if (markerSaved)
			{
				Ped plrPed = Game.Player.Character;

				if (plrPed.Position.DistanceTo(pos) < 2f) return; //don't TP if already near the marker

				if (playSounds && (!holdGoto || !playSoundsWhenStartHold)) gotoMarkerSound.Play();

				Function.Call(Hash.LOAD_SCENE, pos.X, pos.Y, pos.Z);

				if (saveVehicle && wasInVehicle)
				{
					if (plrPed.IsInVehicle()) plrPed.CurrentVehicle.Delete();

					Vehicle veh = World.CreateVehicle(savedVehModel, pos, heading);
					veh.IsEngineRunning = savedVehRunning;
					Function.Call(Hash.SET_​HELI_​BLADES_​SPEED, veh, 1.0f);
					veh.Mods.PrimaryColor = savedVehPrimaryColor;
					veh.Mods.SecondaryColor = savedVehSecondaryColor;
					veh.Mods.Livery = savedVehLivery;
					veh.LandingGearState = savedVehLandingGear;
					Function.Call(Hash.SET_​VEHICLE_​FLIGHT_​NOZZLE_​POSITION_​IMMEDIATE, veh, savedVehNozzlePos);
					veh.Mods.InstallModKit();

					for (int i = 0; i < MOD_TYPES.Length; i++)
					{
						veh.Mods[MOD_TYPES[i]].Index = savedVehModIndices[i];
					}

					plrPed.SetIntoVehicle(veh, VehicleSeat.Driver);
					Function.Call(Hash.FORCE_ROOM_FOR_ENTITY, veh, interior, roomKey);

					if (saveVelocity) veh.Velocity = vehVel;
				}
				else if (takeVehicle && (!takeVehicleIfSaved || wasInVehicle) && plrPed.IsInVehicle())
				{
					Vehicle veh = plrPed.CurrentVehicle;

					veh.Position = pos;
					veh.Heading = heading;
					Function.Call(Hash.FORCE_ROOM_FOR_ENTITY, veh, interior, roomKey);

					if (saveVelocity) veh.Velocity = vehVel;
				}
				else
				{
					plrPed.PositionNoOffset = pos;
					plrPed.Heading = heading;
				}

				Function.Call(Hash.FORCE_ROOM_FOR_ENTITY, plrPed, interior, roomKey);
				GameplayCamera.RelativeHeading = camHeading;
				GameplayCamera.RelativePitch = camPitch;
			}
		}
	}
}