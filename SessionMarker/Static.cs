﻿using GTA;
using GTA.UI;
using System;
using System.Diagnostics;
using System.Drawing;

namespace SessionMarker
{
	class Static : Script
	{
		static bool fadeIn;
		static int time;
		static Stopwatch stopwatch = new Stopwatch();

		CustomSprite sprite = new CustomSprite("./scripts/SessionMarker/static.png", Screen.Resolution, new PointF(0f, 0f), Color.FromArgb(0, Color.WhiteSmoke));
		bool upright;

		public Static()
		{
			Interval = 0;
			Tick += Static_Tick;
		}

		private void Static_Tick(object sender, EventArgs e)
		{
			if (fadeIn)
			{
				upright = !upright;
				int alpha = Math.Min((int)(((float)stopwatch.ElapsedMilliseconds / time) * 255), 255);
				sprite.Color = Color.FromArgb(alpha, Color.White);
				sprite.Rotation = upright ? 0f : 180f;
				sprite.Draw();

				if (stopwatch.ElapsedMilliseconds > time)
				{
					fadeIn = false;
					stopwatch.Reset();
				}
			}
		}

		public static void FadeIn(int time)
		{
			Static.time = time;
			stopwatch.Start();
			fadeIn = true;
		}

		public static void Cancel()
		{
			fadeIn = false;
			stopwatch.Reset();
		}
	}
}
